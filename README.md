Hadoop and Spark installation info
-----------------------------------
The various Hadoop components would be installed at '/usr/lib'. The Hadoop configuration files can be found at '/etc/hadoop/conf'. The various Hadoop services installed will be:
* hadoop-hdfs-namenode
* hadoop-hdfs-datanode
* hadoop-yarn-nodemanager
* hadoop-yarn-resourcemanager
* hadoop-mapreduce-historyserver

Spark will be installed at the location '/usr/lib/spark' and its configuration files will be at the location '/etc/spark/conf'. The various Spark services installed will be:
* spark-master
* spark-worker


JDK installation info
----------------------
The JDK version installed will be JDK 8 u51. Java home will be at location '/opt/jdk1.8.0_51'.


Environment variables
----------------------
* JAVA_HOME will be set in /etc/environment, /etc/hadoop/conf/yarn-env.sh and /etc/hadoop/conf/hadoop-env.sh
* HADOOP_CONF_DIR and YARN_CONF_DIR will be set in /etc/environment


To submit a example Spark job to YARN
--------------------------------------
    # su - hdfs
    $ cd /usr/lib/spark
    $ spark-submit --class org.apache.spark.examples.SparkPi \
        --master yarn-cluster \
        --num-executors 1 \
        --driver-memory 2g \
        --executor-memory 1g \
        --executor-cores 1 \
        --queue thequeue \
        lib/spark-examples.jar \
        10

The job will go through various YARN states, ACCEPTED, RUNNING and FINISHED. The status of the job in the end should be SUCCEEDED.
The computed value of Pi will be printed to the log file.


To view the log file of a job from HDFS
---------------------------------------
    $ yarn logs -applicationId <applicationId>