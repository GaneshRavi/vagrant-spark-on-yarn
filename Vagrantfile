# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # All Vagrant configuration is done here. The most common configuration
  # options are documented and commented below. For a complete reference,
  # please see the online documentation at vagrantup.com.

  # Every Vagrant virtual environment requires a box to build off of.
  config.vm.box = "ubuntu/trusty64"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # config.vm.network "forwarded_port", guest: 18080, host: 18080

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # If true, then any SSH connections made will enable agent forwarding.
  # Default value: false
  # config.ssh.forward_agent = true

  config.ssh.forward_x11 = true

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
     # Don't boot with headless mode
     # vb.gui = true
  
     # Use VBoxManage to customize the VM. For example to change memory:
     vb.customize ["modifyvm", :id, "--memory", "8192"]
	 vb.customize ["modifyvm", :id, "--cpus", 4]
  end
  #
  # View the documentation for the provider you're using for more
  # information on available options.

  # Enable provisioning with CFEngine. CFEngine Community packages are
  # automatically installed. For example, configure the host as a
  # policy server and optionally a policy file to run:
  #
  # config.vm.provision "cfengine" do |cf|
  #   cf.am_policy_hub = true
  #   # cf.run_file = "motd.cf"
  # end
  #
  # You can also configure and bootstrap a client to an existing
  # policy server:
  #
  # config.vm.provision "cfengine" do |cf|
  #   cf.policy_server_address = "10.0.2.15"
  # end

  # Enable provisioning with Puppet stand alone.  Puppet manifests
  # are contained in a directory path relative to this Vagrantfile.
  # You will need to create the manifests directory and a manifest in
  # the file default.pp in the manifests_path directory.
  #
  # config.vm.provision "puppet" do |puppet|
  #   puppet.manifests_path = "manifests"
  #   puppet.manifest_file  = "site.pp"
  # end

  if Vagrant.has_plugin?("vagrant-cachier")
    config.cache.scope = :machine
  end

  $script = <<SCRIPT
    echo Provisioning...
	
	# Downloading and setting up JDK8
	wget --quiet --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u51-b16/jdk-8u51-linux-x64.tar.gz
	sudo tar -zxf jdk-8u51-linux-x64.tar.gz -C /opt/
	rm jdk-8u51-linux-x64.tar.gz
	#sudo tar -zxf /vagrant/jdk-8u51-linux-x64.gz -C /opt/
	sudo ln -f -s /opt/jdk1.8.0_51/bin/* /usr/bin/
	
	# Creating a ENV VARs which will be set for all users
	sudo echo $'\nJAVA_HOME=/opt/jdk1.8.0_51' >> /etc/environment
    
	# Installing other optional utility packages
	sudo apt-get install -y xauth dos2unix vim git
	
	# Adding Cloudera CDH repository
	sudo wget 'http://archive.cloudera.com/cdh5/ubuntu/trusty/amd64/cdh/cloudera.list' -O /etc/apt/sources.list.d/cloudera.list
	curl -s http://archive.cloudera.com/cdh5/ubuntu/precise/amd64/cdh/archive.key | sudo apt-key add -
	# Additional step on Ubuntu Trusty, to avoid name collision of Zookeeper packages
	sudo cp /vagrant/cloudera.pref /etc/apt/preferences.d/cloudera.pref
	
	# Installing and starting up Zookeeper
	sudo apt-get update
	sudo apt-get install -y zookeeper-server
	sudo mkdir -p /var/lib/zookeeper
	sudo chown -R zookeeper /var/lib/zookeeper/
	sudo service zookeeper-server init
	sudo service zookeeper-server start
	
	# Installing Hadoop components
    sudo apt-get install -y hadoop-yarn-resourcemanager
	sudo apt-get install -y hadoop-hdfs-namenode
	sudo apt-get install -y hadoop-yarn-nodemanager hadoop-hdfs-datanode hadoop-mapreduce
	sudo apt-get install -y hadoop-mapreduce-historyserver
	sudo apt-get install -y hadoop-client
	
	# Configuring Hadoop components
    echo Copying provided config files...
	sudo cp -f /vagrant/config/hdfs-site.xml /etc/hadoop/conf/hdfs-site.xml
	sudo cp -f /vagrant/config/mapred-site.xml /etc/hadoop/conf/mapred-site.xml
	sudo cp -f /vagrant/config/core-site.xml /etc/hadoop/conf/core-site.xml
	sudo cp -f /vagrant/config/yarn-site.xml /etc/hadoop/conf/yarn-site.xml
	sudo cp -f /vagrant/config/yarn-env.sh /etc/hadoop/conf/yarn-env.sh
	sudo cp -f /vagrant/config/hadoop-env.sh /etc/hadoop/conf/hadoop-env.sh
	sudo chmod 774 /etc/hadoop/conf/yarn-env.sh
	sudo chmod 774 /etc/hadoop/conf/hadoop-env.sh
	sudo dos2unix /etc/hadoop/conf/yarn-env.sh /etc/hadoop/conf/hadoop-env.sh
	
	# The below directory should be created for namenode, with appropriate permissions
	sudo mkdir -p /var/lib/hadoop-hdfs/cache/hdfs/dfs/name
	sudo chown -R hdfs:hadoop /var/lib/hadoop-hdfs/cache
	sudo su - hdfs -c "hdfs namenode -format"
	
	sudo /etc/init.d/hadoop-hdfs-namenode restart
	sudo /etc/init.d/hadoop-hdfs-datanode restart
	
	# The required directory should be created in HDFS with appropriate permissions for mapred user, only then logs can be written.
	sudo su - hdfs -c "hdfs dfs -mkdir -p /tmp/logs"
	sudo su - hdfs -c "hdfs dfs -chown -R mapred:hadoop /tmp/"
	
	sudo /etc/init.d/hadoop-mapreduce-historyserver restart
	sudo /etc/init.d/hadoop-yarn-nodemanager restart
	sudo /etc/init.d/hadoop-yarn-resourcemanager restart
	
	# Installing Spark and setting required ENV VARs
	sudo apt-get install spark-core spark-master spark-worker spark-python
	sudo echo $'\nHADOOP_CONF_DIR=/etc/hadoop/conf' >> /etc/environment
	sudo echo $'\nYARN_CONF_DIR=/etc/hadoop/conf' >> /etc/environment
	
    date > /etc/vagrant_provisioned_at
SCRIPT

  config.vm.provision "shell",
    inline: $script

  config.vm.define "sparkA" do |spark|
    spark.vm.hostname = "sparkA"
    spark.vm.network "public_network", ip: "192.168.0.100"
  end

  # Enable provisioning with chef solo, specifying a cookbooks path, roles
  # path, and data_bags path (all relative to this Vagrantfile), and adding
  # some recipes and/or roles.
  #
  # config.vm.provision "chef_solo" do |chef|
  #   chef.cookbooks_path = "../my-recipes/cookbooks"
  #   chef.roles_path = "../my-recipes/roles"
  #   chef.data_bags_path = "../my-recipes/data_bags"
  #   chef.add_recipe "mysql"
  #   chef.add_role "web"
  #
  #   # You may also specify custom JSON attributes:
  #   chef.json = { mysql_password: "foo" }
  # end

  # Enable provisioning with chef server, specifying the chef server URL,
  # and the path to the validation key (relative to this Vagrantfile).
  #
  # The Opscode Platform uses HTTPS. Substitute your organization for
  # ORGNAME in the URL and validation key.
  #
  # If you have your own Chef Server, use the appropriate URL, which may be
  # HTTP instead of HTTPS depending on your configuration. Also change the
  # validation key to validation.pem.
  #
  # config.vm.provision "chef_client" do |chef|
  #   chef.chef_server_url = "https://api.opscode.com/organizations/ORGNAME"
  #   chef.validation_key_path = "ORGNAME-validator.pem"
  # end
  #
  # If you're using the Opscode platform, your validator client is
  # ORGNAME-validator, replacing ORGNAME with your organization name.
  #
  # If you have your own Chef Server, the default validation client name is
  # chef-validator, unless you changed the configuration.
  #
  #   chef.validation_client_name = "ORGNAME-validator"
end